﻿# Host: 127.0.0.1  (Version 5.5.5-10.1.35-MariaDB)
# Date: 2018-11-26 19:30:25
# Generator: MySQL-Front 6.1  (Build 1.21)


#
# Structure for table "usuario"
#

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `IDUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Usuario` varchar(100) NOT NULL,
  `E_mail` varchar(100) NOT NULL,
  `Senha_Usuario` varchar(50) NOT NULL,
  PRIMARY KEY (`IDUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "usuario"
#

INSERT INTO `usuario` VALUES (1,'Rafael','teste@teste.com','123'),(2,'eu','eu@eu.com','21313'),(3,'RAFAEL FONTANA SILVA','rafaelfontana20@gmail.com','123'),(4,'pedro','pedro@gmail.com','pedro123');

#
# Structure for table "projeto"
#

DROP TABLE IF EXISTS `projeto`;
CREATE TABLE `projeto` (
  `IDProjeto` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Projeto` varchar(100) NOT NULL,
  `Descricao_Projeto` varchar(500) NOT NULL,
  `Responsavel_Projeto` varchar(100) NOT NULL,
  `Comentario_Projeto` varchar(250) DEFAULT NULL,
  `IDUsuario` int(11) NOT NULL,
  PRIMARY KEY (`IDProjeto`),
  KEY `fk_Projetos_usuario` (`IDUsuario`),
  CONSTRAINT `fk_Projetos_usuario` FOREIGN KEY (`IDUsuario`) REFERENCES `usuario` (`IDUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

#
# Data for table "projeto"
#

INSERT INTO `projeto` VALUES (1,'Projeto X','O Projeto x é um projeto espacial ','Rafael','Esse projeto é top',1),(5,'Projeto A','Esse projeto consiste em mudar o mundo.','Rafael','Executar todas as atividade conforme planejado',3),(7,'Proejeto B','addada','Rafael','gsGSGS',1),(8,'Projeto C','O projeto Ã© bem louco.','pedro','NÃ£o sei se vai dar certo',4),(9,'Semear','Ã‰ um projeto para atender pessoas carentes','pedro','Um projeto de vida',4);

#
# Structure for table "atividade"
#

DROP TABLE IF EXISTS `atividade`;
CREATE TABLE `atividade` (
  `IDAtividade` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Atividade` varchar(100) NOT NULL,
  `Responsavel_Atividade` varchar(100) NOT NULL,
  `Descricao_Atividade` varchar(500) NOT NULL,
  `Data_Incio` date NOT NULL,
  `Data_Termino` date NOT NULL,
  `Comentario_Atividade` varchar(250) DEFAULT NULL,
  `IDProjeto` int(11) NOT NULL,
  PRIMARY KEY (`IDAtividade`),
  KEY `fk_Projetos_atividades` (`IDProjeto`),
  CONSTRAINT `fk_Projetos_atividades` FOREIGN KEY (`IDProjeto`) REFERENCES `projeto` (`IDProjeto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "atividade"
#

