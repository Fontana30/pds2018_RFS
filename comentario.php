<?php
require("conexao.php");
 
	$result_msg_atividade = "select * from projeto";
	$result = mysqli_query($conn, $result_msg_atividade);


?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
		<meta charset="utf-8">
		<title>Comentarios</title>
			
	</head>
	

	<body>
	<body background="fundo.jpg">
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <span class="navbar-brand mb-0 h1" >Gestiōne</span>
  
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="home.php">Home</a> 
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="form_projeto.php">Projetos</a> 
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="form_atividade.php">Atividades</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="logout.php">Sair</a>
      </li>
    </ul>
  </div>
</nav>
	<br>
	<br>
	
	 <div align="center">
	
	<div class="card bg-light mb-3" style="max-width: 30rem;">
        <div class="bgWhite maxWidth padding">
		<h1> Comentários </h1>
		<br>
            <form method="post" name="frmComentario" id="frmComentario">
                <ul>
                    <li class="textoComentario">Nome</li>
                    <li class="break"><input type="text" name="txtNome" id="txtNome" /></li>

                    <li class="textoComentario">Comentário</li>
                    <li class="break"><textarea name="txtComentario" id="txtComentario"></textarea></li>

                    
                    <li class="break"><input type="submit" name="btnSubmit" id="btnSubmit" value="Comentar" class="btnSubmit" /></li>
                </ul>
            </form>
        </div>
		</div>
		</div>
        <br />
        <div class="bgWhite maxWidth padding">
            <?php
            foreach ($listaComentario as $comentario) {
                ?>
                <div class="dvComentario">
                    <p class="fontBold"><?= $comentario->getNome() ?></p>
                    <p><?= $comentario->getMensagem() ?></p>
                        <br />
                    <div class="line"></div>
                    <br />
                </div>
                <?php
            }
            ?>
        </div>
        <br />
        <br />
	
	
	
	</body>
</html>